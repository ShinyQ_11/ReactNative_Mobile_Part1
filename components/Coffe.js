import React, {Component} from 'react';
import {View, Text} from 'react-native';

class Coffe extends Component{

    render(){

        const{type, bean, volume, info} = this.props;

        return(
        <View>
            <Text>
            Coffe Ini jenis {type} dengan biji kopi 
            {bean} dan isinya {volume} liter , rasanya {info.taste}, 
             warnanya {info.color}.
            </Text>
        </View>
        )
    }


}

export default Coffe;
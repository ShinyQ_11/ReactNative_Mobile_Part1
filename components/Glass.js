import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Coffe from './Coffe'

export default class Glass extends Component{

    render(){

        const info = {

            color: "Warna Coklat Muda ",
            taste: "Luar Biasa"

        };

        return(
        <View>
            <Text>Glass</Text>
            <Coffe type="Espresso" bean="Arabica" volume={2} info={info}/>
        </View>
        )
    }


}
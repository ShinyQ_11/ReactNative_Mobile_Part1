import React, {Component} from 'react';
import {ListView, Text, View} from 'react-native';

export default class MyAnimeList extends Component{

    animes = [
        'Naruto',
        'Attack On Titan',
        'Dragon Ball Z-Kai',
        'Sword Art Online'
    ]

    constructor(){
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1 , r2) => r1 != r2});
        this.state = {
           dataSource: ds.cloneWithRows(this.animes)
        }
    }

    render(){
        return(
            <View>

            <Text>Dengan Data Source : </Text>

            <ListView
                dataSource={this.state.dataSource}
                renderRow={(rowData)=> <Text>{rowData}</Text>}
                />

                <Text></Text>
            
            <Text>Dengan Data Maps : </Text>

            {this.animes.map((anime, key)=> <Text>{anime}</Text>)}

            </View>

            
        )
    }

}
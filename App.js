import React, {Component} from 'react';
import {View, Text, TouchableHighlight, StyleSheet} from 'react-native';
import Glass from './components/Glass';
import Cup from './components/Cup';
import MyAnimeList from './MyAnimeList';

export default class Index extends Component{



    render(){


        return( 
        <View style={styles.container}>
            <MyAnimeList/>
        </View>   
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },

    header:{
        flex: 0.3,
        backgroundColor: "green",
        justifyContent: "center"
    },

    footer:{
        flex: 3,
        backgroundColor: "red",
        justifyContent: "center",

    },
    footerText:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#FFF',
        alignSelf: "center",
    },

});


